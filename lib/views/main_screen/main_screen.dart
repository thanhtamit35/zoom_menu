import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:untitled1/views/home_screen/home_screen.dart';
import 'package:untitled1/views/main_screen/main_screen_controller.dart';

class MainScreen extends StatelessWidget {
  MainScreen({super.key});

  final mainScreenController = Get.put(MainScreenController());

  @override
  Widget build(BuildContext context) {
    return GetBuilder<MainScreenController>(builder: (controller) {
      return Scaffold(
        appBar: AppBar(
          leading: Container(),
          title: const Center(child: Text('Bạn bè chơi golf', ),),
          actions: [
            IconButton(onPressed: (){}, icon: const Icon(Icons.notifications),),
            IconButton(onPressed: (){}, icon: const Icon(Icons.account_circle_rounded),),
          ],
        ),
        body: PageView(
          physics: const NeverScrollableScrollPhysics(),
          controller: mainScreenController.pageController,
          children: [
            HomeScreen(),
            Container(),
            Container(
              color: Colors.blue,
              child: const Center(
                child: Text('Page 3'),
              ),
            ),
            Container(),
            Container(
              color: Colors.blue,
              child: const Center(
                child: Text('Page 5'),
              ),
            ),
          ],
        ),
        bottomNavigationBar: SizedBox(
          height: 80,
          child: BottomNavigationBar(
            currentIndex: mainScreenController.selectedIndex,
            onTap: (value) {
              mainScreenController.setSelectedIndex(value);
              mainScreenController.jumToPage(value);
            },
            items:  [
              BottomNavigationBarItem(
                icon: const Icon(Icons.person),
                label: 'Friends',
                backgroundColor: Colors.green.shade900,
              ),
              BottomNavigationBarItem(
                icon: const Icon(CupertinoIcons.bolt),
                label: 'Flash',
                backgroundColor: Colors.green.shade900,
              ),
              BottomNavigationBarItem(
                icon: const Icon(Icons.golf_course),
                label: 'Friends',
                backgroundColor: Colors.green.shade900,
              ),
              BottomNavigationBarItem(
                icon: const Icon(Icons.diamond_outlined),
                label: 'Awards',
                backgroundColor: Colors.green.shade900,
              ),
              BottomNavigationBarItem(
                icon: const Icon(CupertinoIcons.chat_bubble_fill),
                label: 'Friends',
                backgroundColor: Colors.green.shade900,
              ),
            ],
          ),
        ),
      );
    },);
  }
}
