import 'package:flutter/material.dart';
import 'package:flutter_zoom_drawer/flutter_zoom_drawer.dart';
import 'package:get/get.dart';

class MainScreenController extends GetxController {
  int selectedIndex = 0;
  PageController pageController = PageController();

  final zoomDrawerController = ZoomDrawerController();

  void toggleDrawer() {
    print("Toggle drawer");
    zoomDrawerController.toggle?.call();
    update();
  }

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    pageController.dispose();
  }

  void jumToPage(int value){
    pageController.jumpToPage(value);
    update();
  }

  void setSelectedIndex(int value){
    selectedIndex = value;
    update();
  }
}