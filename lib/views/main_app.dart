import 'package:flutter/material.dart';
import 'package:flutter_zoom_drawer/flutter_zoom_drawer.dart';
import 'package:get/get.dart';
import 'package:untitled1/views/main_screen/main_screen.dart';
import 'package:untitled1/views/main_screen/main_screen_controller.dart';
import 'package:untitled1/views/menu_screen/menu_screen.dart';

class MainApp extends StatelessWidget {
  final mainScreenController = Get.put(MainScreenController());

  MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<MainScreenController>(
      builder: (_) => ZoomDrawer(
        controller: _.zoomDrawerController,
        menuScreen: MenuScreen(),
        mainScreen: MainScreen(),
        borderRadius: 24.0,
        showShadow: false,
        angle: 0.0,
        drawerShadowsBackgroundColor: Colors.grey.shade300,
        slideWidth: MediaQuery.of(context).size.width * 0.65,
      ),
    );
  }
}
