import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:untitled1/views/home_screen/home_controller.dart';
import 'package:untitled1/views/main_screen/main_screen_controller.dart';

class HomeScreen extends StatelessWidget {
  HomeScreen({super.key});
  final homeController = Get.put(HomeController());
  final mainScreenController = Get.put(MainScreenController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.blueGrey,
        child: Center(
          child: ElevatedButton(
            onPressed: mainScreenController.toggleDrawer,
            child: Text("Toggle Drawer"),
          ),
        ),
      ),
    );
  }
}
