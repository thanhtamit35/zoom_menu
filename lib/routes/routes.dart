import 'package:get/get.dart';
import 'package:untitled1/views/main_app.dart';
import 'package:untitled1/routes/route_name.dart';
import 'package:untitled1/views/splash/splash_screen.dart';

class AppRoutes {
  static appRoutes() => [
    GetPage(
      name: RouteName.splashScreen,
      page: () => const SplashScreen(),
      transitionDuration: const Duration(milliseconds: 200),
      transition: Transition.leftToRightWithFade,
    ),
    GetPage(
      name: RouteName.mainAppScreen,
      page: () => MainApp(),
      transitionDuration: const Duration(milliseconds: 200),
      transition: Transition.leftToRightWithFade,
    ),
  ];
}